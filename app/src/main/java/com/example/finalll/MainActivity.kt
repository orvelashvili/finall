package com.example.finalll

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import androidx.navigation.findNavController as findNavController

class MainActivity : AppCompatActivity() {
    private lateinit var loader: ProgressBar
    private lateinit var btnGetStarted: Button
    private val auth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_splash)







        btnGetStarted = findViewById(R.id.btnGetStarted)
        loader = findViewById(R.id.loader)

        Handler().postDelayed({
            loader.visibility = View.INVISIBLE
            btnGetStarted.visibility = View.VISIBLE

        }, 1000)

        btnGetStarted.setOnClickListener{
            if(auth.currentUser!=null){
                startActivity(Intent(this , ProfileActivity::class.java))
                finish()
            }else{
                startActivity(Intent(this , LoginActivity::class.java))
                finish()
            }
        }
    }

}