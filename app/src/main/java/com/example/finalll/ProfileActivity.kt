package com.example.finalll


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class ProfileActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_profile)








        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.Menu)

        val controller = findNavController(R.id.nav_host_fragment_container)
        val appBarConfig = AppBarConfiguration(setOf(
            R.id.profileFragment,
            R.id.homeFragment,
            R.id.detailActivity,


            ))

        setupActionBarWithNavController(controller, appBarConfig)
        bottomNavigationView.setupWithNavController(controller)
    }
}