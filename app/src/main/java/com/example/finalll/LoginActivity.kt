package com.example.finalll

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private lateinit var login2: Button
    private lateinit var registerPassword: TextInputEditText
    private lateinit var registerEmail: TextInputEditText
    private lateinit var register2: Button
    private lateinit var reset: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        if (FirebaseAuth.getInstance().currentUser != null) {
            goToProfile()
        }
        setContentView(R.layout.activity_login)
        init()
        registerListeners()

        /* აქ შემოსატანი მაქვს აიდები , კოდები მაქვს გასაწერი და დასამახსოვრებელი */
    }

    private fun init() {
        login2 = findViewById(R.id.login2)
        registerPassword = findViewById(R.id.registerPassword)
        registerEmail = findViewById(R.id.registerEmail)
        register2 = findViewById(R.id.register2)
        reset = findViewById(R.id.reset)


    }

    private fun registerListeners() {
        login2.setOnClickListener() {
            val email = registerEmail.text.toString()
            val password = registerPassword.text.toString()

            if (email.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "მეილი ან პაროლი ცარიელია", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        goToProfile()
                    } else {
                        Toast.makeText(this, "რეგისტრაცია წარუმატებელია", Toast.LENGTH_SHORT).show()
                    }
                }

        }
        register2.setOnClickListener() {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)

        }
        reset.setOnClickListener(){
            val intent = Intent(this,LoginActivity::class.java)
            startActivity(intent)

        }



    }

    private fun goToProfile() {
        startActivity(Intent(this, ProfileActivity::class.java))
        finish()
    }
}

