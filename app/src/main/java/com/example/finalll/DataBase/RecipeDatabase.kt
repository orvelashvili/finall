package com.example.finalll.DataBase

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.finalll.Fragments.SplashActivity
import com.example.finalll.entities.Category
import com.example.finalll.entities.CategoryItems
import com.example.finalll.entities.MealsItems
import com.example.finalll.entities.Recipes
import com.example.finalll.entities.converter.CategoryListConverter
import com.example.finalll.entities.converter.MealListConverter


