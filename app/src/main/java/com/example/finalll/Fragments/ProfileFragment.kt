package com.example.finalll.Fragments

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide.init
import com.example.finalll.R
import com.google.firebase.auth.FirebaseAuth

class ProfileFragment: Fragment(R.layout.fragment_profile) {

    private val auth = FirebaseAuth.getInstance()
    private lateinit var logout: Button
    private lateinit var person: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



        logout.setOnClickListener(){
            auth.signOut()
            Intent(activity, ProfileFragment::class.java).also {
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(it)
            }
        }



    }




}