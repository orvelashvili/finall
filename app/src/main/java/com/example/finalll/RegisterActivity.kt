package com.example.finalll

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private lateinit var bottomRegister: Button
    private lateinit var bottomLogin: Button
    private lateinit var loginPassword2: TextInputEditText
    private lateinit var loginEmail2: TextInputEditText
    private lateinit var loginName: TextInputEditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_register)

        init()
        registerListeners()
    }

    private fun init(){
        bottomRegister = findViewById(R.id.bottomRegister)
        bottomLogin = findViewById(R.id.bottomLogin)
        loginName = findViewById(R.id.loginName)
        loginPassword2 = findViewById(R.id.loginPassword2)
        loginEmail2 = findViewById(R.id.loginEmail2)
    }

    private fun registerListeners(){
        bottomLogin.setOnClickListener{
            startActivity(Intent(this, LoginActivity::class.java))
        }
        bottomRegister.setOnClickListener{
            val email = loginEmail2.text.toString()
            val password = loginPassword2.text.toString()

            if(email.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "Email or password is blank", Toast.LENGTH_SHORT).show()
                return@setOnClickListener


            }

            FirebaseAuth.getInstance().createUserWithEmailAndPassword( email, password)

                .addOnCompleteListener { task ->
                    if (task.isSuccessful){
                        startActivity(Intent(this, LoginActivity::class.java))
                        finish()
                    } else {
                        Toast.makeText(this, "The operation failed", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }

}
